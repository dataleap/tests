package pt.edp.renewable.pvr

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import java.util.GregorianCalendar
import java.util.Calendar
import java.util.Date
import java.io.PrintWriter
import java.io.File
import java.io.FileWriter
import java.io.FileOutputStream
import java.io.Writer

object DataGeneration {
    val jobName = "MainExample" 
    val conf = new SparkConf().setAppName(jobName)
    val sc = new SparkContext(conf) //Zeppelin creates this variable
      
      
  class TimeSerie(val id: Int, val date: String, val value: Double) {
    def toCsv = id.toString() + "," + date + "," + "%.2f".format(value)
  }
  
    object MeteoF {
    private val format = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    private val start = 1104537600000l
    private val end = 1461542400000l
    def toCsv(lat: Double, lng: Double, date: String, value: Double) = "%.2f".format(lat) + "," + "%.2f".format(lng) + "," + date + "," + "%.2f".format(value)
    def generate_data(lat: Double, lng: Double, start: Long = start, end: Long = end) = {
      val a = for (f <- List.range(start, end, 600000)) yield new Date(f)
      a.map { date => toCsv(lat, lng, format.format(date), sumTempComponent(date, lat, lng)) }
    }

    val nDelay = 6
    val timeBetweenRecords = 600000

    def fff(t: Double, e: (Double, Double)) = { e._1 * (1 - t) + t * e._2 }

    def dayPercent(x: Date) = Math.sin(Math.PI * (x.getHours() / 24.0 + x.getMinutes() / 60.0 / 24.0))

    def tempDayComponent(x: Date) = {
      val dayMaxMin = (-10d, 0d)
      fff(dayPercent(x), dayMaxMin)
    }

    def tempYearlyComponent(x: Date) = {
      val tm = Seq(14.2, 15.8, 18, 20, 22, 27, 28, 29, 27, 22, 17, 14, 14.2)
      val m = x.getMonth()
      val d = x.getDate()
      fff(d / 31f, (tm(m), tm(m + 1)))
    }

    def tempVariationComponent(x: Date) = {
      val varianceMaxMin = (3d, -3d)
      def oldDates(date: Date) =
        for (a <- 1 to nDelay) yield date.getTime() + (-a * timeBetweenRecords) //format.format(new Date(f))
      def mean(s: Seq[Long]) = {
        s.map { x => (x / 100000f) % 100 }.sum / nDelay
      }
      fff(mean(oldDates(x)) / 100f, varianceMaxMin)
    }
    def latComponent(lat: Double) = 2f / 10f * Math.atan(-lat + 38.4) + 1
    def longComponent(long: Double) = 1f / 10f * Math.atan(-long - 8) + 1
    def sumTempComponent(x: Date, lat: Double, long: Double) =
      (tempYearlyComponent(x) + tempDayComponent(x)) * latComponent(lat) * longComponent(long) + tempVariationComponent(x)

    def energyUsed(x: Date, temp: Double, magic: Int ) = {
      def energyFunction(x: Double): Double = {
        (0.31693780745315592 // 3.1693780745315592e-001 * x^0
          - 0.14921830218566329 * x //-1.4921830218566329e-001 * x^1
          + 0.066911731405542449 * x * x // 6.6911731405542449e-002 * x^2
          - 0.0083924904177081795 * x * x * x //-8.3924904177081795e-003 * x^3
          + 0.00041553371069717748 * x * x * x * x // 4.1553371069717748e-004 * x^4
          - 0.0000071679782062421749 * x * x * x * x * x //-7.1679782062421749e-006 * x^5
          )
      }
      val calendar = new GregorianCalendar(); calendar.setTime(x)
      val dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)
      val hotfix = (x.getHours() + x.getMinutes() / 60d) / 24d * 23.1
      val energyPercent = energyFunction(hotfix)
      val baseTemp = Math.pow(Math.abs(temp - 22d), 2) * 0.002 + 1
      var energy = energyPercent * baseTemp
      dayOfWeek match {
        case 1 => energy *= 0.6 //domingo
        case 2 => energy *= 0.95 //segunda
        case 3 => energy *= 1.1
        case 4 => energy *= 1.11
        case 5 => energy *= 1.1
        case 6 => energy *= 1.12
        case 7 => energy *= 0.65
      }
      val escalarMaxMin = (0d, 1d) //FIXME
      energy = fff(energy, escalarMaxMin)
      //println(energyPercent + "  \t" + baseTemp + "  \t" + energy)
      energy * magic
    }
  }

  object TimeSerieF {
    private val format = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    private val start = 1104537600000l
    private val end = 1461542400000l
    def generate_data(id: Int, start: Long = start, end: Long = end, magic: Int) = {
      val a = for (f <- List.range(start, end, 600000)) yield new Date(f)
      val localization = latlng(id)
      a.map { date => new TimeSerie(id, format.format(date), MeteoF.energyUsed(date, MeteoF.sumTempComponent(date, localization._1, localization._2), magic)) }
    }
    def latlng(id: Int) = {
      (37 + ((id / 20) % 48) * .1, -9 + (id % 20) * .1)
    }
  }

  def streaming(measurement_writer: Writer, meteo_writer: Writer, start: Long, end: Long, ids: List[Int]) {
    val t = for (lng <- (-9.0 to -7.0 by .1)) yield for (lat <- (37.0 to 41.8 by .1)) yield (lat, lng)
    val meteoIds = t.flatten

    val dates: List[Long] = for (f <- List.range(start, end, 600000)) yield f

    //loop
    dates.foreach { date =>
      Thread.sleep(1000);
      println("Streaming date: " + date);
      val timeseries = ids.flatMap { x => TimeSerieF.generate_data(x, date, date + 1l, magic=11132) }.map { _.toCsv } //FIXME HOTFIX date+1l
      val meteoTimeseries = meteoIds.flatMap { x => MeteoF.generate_data(x._1, x._2, date, date + 1l) } //FIXME HOTFIX date+1l

      timeseries.foreach { x => measurement_writer.write(x + "\n") }
      measurement_writer.flush()

      meteoTimeseries.foreach { x => meteo_writer.write(x + "\n") }
      meteo_writer.flush()
    }
  }

  def runOnSparkBatch {
    val hadoopConf = new org.apache.hadoop.conf.Configuration()
    val hdfs = org.apache.hadoop.fs.FileSystem.get(new java.net.URI("hdfs://nameservice1"), hadoopConf)

    val EXECUTORS = 13
    val ids = sc.parallelize(List.range(1, 35000)).coalesce(EXECUTORS * 10, true)
    val timeseries = ids.flatMap { x => TimeSerieF.generate_data(x, magic=11127) }.map { _.toCsv }

    val t = for (lng <- (-9.0 to -7.0 by .1)) yield for (lat <- (37.0 to 41.8 by .1)) yield (lat, lng)
    val meteoIds = sc.parallelize(t.flatten).coalesce(EXECUTORS * 10, true)
    val meteoTimeseries = meteoIds.flatMap { x => MeteoF.generate_data(x._1, x._2) }

    try { hdfs.delete(new org.apache.hadoop.fs.Path("hdfs://nameservice1/user/zeppelin/measurement.csv"), true) } catch { case _: Throwable => {} }
    timeseries.saveAsTextFile("hdfs://nameservice1/user/zeppelin/measurement.csv")

    try { hdfs.delete(new org.apache.hadoop.fs.Path("hdfs://nameservice1/user/zeppelin/meteo.csv"), true) } catch { case _: Throwable => {} }
    meteoTimeseries.saveAsTextFile("hdfs://nameservice1/user/zeppelin/meteo.csv")
  }

  def runOnLocalMachineStreaming {
    val path = System.getProperty("java.io.tmpdir") + "DataGeneration\\"
    if (new File(path).mkdir()) println("New Folder: " + path)
    val file_measurement = new File(path + "measurement.csv")
    val file_meteo = new File(path + "meteo.csv")
    println("file_measurement path: " + file_measurement.getAbsolutePath.toString())
    println("file_meteo path: " + file_meteo.getAbsolutePath.toString())
    file_measurement.delete()
    file_meteo.delete()

    val pw_measurement = new PrintWriter(new FileOutputStream(file_measurement, true)) /* append = true */
    val pw_meteo = new PrintWriter(new FileOutputStream(file_meteo, true)) /* append = true */

    val ids = List.range(1, 35000)
    val start = 1356998400000l
    val end = 1461542400000l
    streaming(pw_measurement, pw_meteo, start, end, ids)

    pw_measurement.close
    pw_meteo.close
  }
}
